interface OptionProps {
  alt: string;
  className?: string;
  onClick?: () => void;
  src: any;
}

const Option = (props: OptionProps) => {
  return <img {...props} />;
};

export default Option;

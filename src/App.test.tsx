import { fireEvent, render, screen } from "@testing-library/react";
import App from "./App";

test("renders initial title", () => {
  render(<App />);
  const title = screen.getByText(/choose one/i);
  const options = screen.getAllByRole("img");
  expect(title).toBeInTheDocument();
  expect(options.length).toEqual(3);
});

test("shows result once option chosen", async () => {
  render(<App />);
  const rock = screen.getAllByRole("img")[0];
  fireEvent.click(rock);
  const btn = await screen.findByRole("button");
  const vs = await screen.findByText(/vs/i);
  expect(btn).toBeInTheDocument();
  expect(vs).toBeInTheDocument();
});

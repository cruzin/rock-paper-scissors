export const getClassNames = (classes: Record<string, boolean>) =>
  Object.entries(classes)
    .filter(([key, value]) => value)
    .map(([key, value]) => key)
    .join(" ");

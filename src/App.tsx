import { useEffect, useState } from "react";
import { useTransition, animated } from "react-spring";

import "./App.css";
import Option from "./components/option";
import { getClassNames } from "./utilities/classNames";

import paper from "./art/paper.png";
import rock from "./art/rock.png";
import scissors from "./art/scissors.png";

const { floor, random } = Math;

enum Winners {
  "USER" = "user",
  "COMPUTER" = "computer",
  "TIE" = "tie",
}

type ValidChoice = "0" | "1" | "2";

interface Choice {
  alt: string;
  src: any;
}

const choices: Choice[] = [
  {
    alt: "rock",
    src: rock,
  },
  {
    alt: "paper",
    src: paper,
  },
  {
    alt: "scissors",
    src: scissors,
  },
];

function App() {
  const [userChoice, setUserChoice] = useState<ValidChoice | null>();
  const [compChoice, setCompChoice] = useState<ValidChoice | null>();
  const [winner, setWinner] = useState<Winners | null>();
  const randomChoice = () => floor(random() * 3).toString() as ValidChoice;

  const fadeIn = useTransition(winner, {
    config: {
      duration: 300,
    },
    from: { opacity: 0 },
    enter: { opacity: 1 },
  });

  const makeChoices = (num: ValidChoice) => {
    setUserChoice(num);
    setCompChoice(randomChoice());
  };

  const reset = () => {
    setUserChoice(null);
    setCompChoice(null);
    setWinner(null);
  };

  useEffect(() => {
    if (!Boolean(userChoice) && !Boolean(compChoice)) return;

    if (userChoice === compChoice) {
      setWinner(Winners.TIE);
    } else if (userChoice === "0" && compChoice === "2") {
      setWinner(Winners.USER);
    } else if (compChoice === "0" && userChoice === "2") {
      setWinner(Winners.COMPUTER);
    } else if (Number(compChoice) > Number(userChoice)) {
      setWinner(Winners.COMPUTER);
    } else {
      setWinner(Winners.USER);
    }
  }, [userChoice, compChoice]);

  return (
    <div className="App">
      {!userChoice && (
        <>
          <h2>Choose One</h2>
          <div className="choices">
            <Option {...choices[0]} onClick={() => makeChoices("0")} />
            <Option {...choices[1]} onClick={() => makeChoices("1")} />
            <Option {...choices[2]} onClick={() => makeChoices("2")} />
          </div>
        </>
      )}
      {fadeIn(
        (style, winner) =>
          winner && (
            <animated.div style={style}>
              {winner !== "tie" ? (
                <h2>{winner.toUpperCase()} wins!</h2>
              ) : (
                <h2>It's a tie!</h2>
              )}
              <div className="result">
                <Option
                  {...choices[Number(userChoice)]}
                  className={getClassNames({
                    "user-choice": true,
                    winner: winner === Winners.USER,
                  })}
                />
                <span>vs</span>
                <Option
                  {...choices[Number(compChoice)]}
                  className={getClassNames({
                    "computer-choice": true,
                    winner: winner === Winners.COMPUTER,
                  })}
                />
              </div>
              <button onClick={reset}>Play again</button>
            </animated.div>
          )
      )}
    </div>
  );
}

export default App;
